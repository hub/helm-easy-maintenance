# What it is

A minimal helm package that allow you to set up a nginx indicating that a maintenance in ongoing.

# Try it
```shell
  # minimal
  helm upgrade --install maintenance ./easy-maintenance/ --set ingressClassName=internal
  # complete
  helm upgrade --install maintenance ./easy-maintenance/ \
    --set ingressClassName=internal \
    --set host=easy-maintenance.dev.pasteur.cloud \
    --set email=ada.lovelace@pasteur.fr \
    --set "teamName=Ada's team" \
    --set "untilWhen=Tomorrow\, 12am" \
    --set "toolName=MyTool"
  # complete in values.example.yaml
  helm upgrade --install maintenance ./easy-maintenance/ --values ./easy-maintenance/values.example.yaml
```

Don't forget `ingressClassName=internal` to use it **only** inside Pasteur, and without DNS declaration.

# Use it
```shell
  helm upgrade --install maintenance ./easy-maintenance/ \
    --set host=my-app.pasteur.cloud \
    --set email=ada.lovelace@pasteur.fr \
    --set "teamName=Ada's team" \
    --set "untilWhen=Tomorrow\, 12am" \
    --set "toolName=MyTool"
```

# limitation

If an ingress is already there, you will not be able to apply it on the same url. 
We recommend you to edit the current ingress to the host is my-app-bis.pasteur.cloud before
applying with 
```shell
helm upgrade --install maintenance ./easy-maintenance/ --set host=my-app-bis.pasteur.cloud
```